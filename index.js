const chalk = require('chalk');

for (let i = 0; i < 1000; i++) {
  console.log('\u001b[33mHello World\u001b[39m');

  console.log(chalk.reset('This is reset'));
  console.log(chalk.bold('This is bold'));
  console.log(chalk.dim('This is dim'));
  console.log(chalk.italic('This is italic'));
  console.log(chalk.underline('This is underline'));
  console.log(chalk.inverse('This is inverse'));
  console.log(chalk.hidden('This is hidden'));
  console.log(chalk.strikethrough('This is strikethrough'));
  console.log(chalk.visible('This is visible'));

  console.log(chalk.black('This is black'));
  console.log(chalk.red('This is red'));
  console.log(chalk.green('This is green'));
  console.log(chalk.yellow('This is yellow'));
  console.log(chalk.blue('This is blue'));
  console.log(chalk.magenta('This is magenta'));
  console.log(chalk.cyan('This is cyan'));
  console.log(chalk.white('This is white'));
  console.log(chalk.gray('This is gray'));
  console.log(chalk.redBright('This is redBright'));
  console.log(chalk.greenBright('This is greenBright'));
  console.log(chalk.yellowBright('This is yellowBright'));
  console.log(chalk.blueBright('This is blueBright'));
  console.log(chalk.magentaBright('This is magentaBright'));
  console.log(chalk.cyanBright('This is cyanBright'));
  console.log(chalk.whiteBright('This is whiteBright'));

  console.log(chalk.bgBlack('This is bgBlack'));
  console.log(chalk.bgRed('This is bgRed'));
  console.log(chalk.bgGreen('This is bgGreen'));
  console.log(chalk.bgYellow('This is bgYellow'));
  console.log(chalk.bgBlue('This is bgBlue'));
  console.log(chalk.bgMagenta('This is bgMagenta'));
  console.log(chalk.bgCyan('This is bgCyan'));
  console.log(chalk.bgWhite('This is bgWhite'));
  console.log(chalk.bgBlackBright('This is bgBlackBright'));
  console.log(chalk.bgRedBright('This is bgRedBright'));
  console.log(chalk.bgGreenBright('This is bgGreenBright'));
  console.log(chalk.bgYellowBright('This is bgYellowBright'));
  console.log(chalk.bgBlueBright('This is bgBlueBright'));
  console.log(chalk.bgMagentaBright('This is bgMagentaBright'));
  console.log(chalk.bgCyanBright('This is bgCyanBright'));
  console.log(chalk.bgWhiteBright('This is bgWhiteBright'));

  console.log(chalk.hex('#673ab7').bold('This is deep purple'));
  console.log(chalk.bgHex('#673ab7').bold('This is deep purple background'));
}

console.log(chalk.supportsColor);
console.log('FORCE_COLOR : ', process.env.FORCE_COLOR);
